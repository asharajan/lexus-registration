 <footer class="footer">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <hr>
            <p>Copyright &copy; Lexus 2016</p>
          </div>
        </div>
      </div>
    </footer>

    <script src="<?php echo base_url(); ?>assets/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/script.js?v=<?php echo filemtime(FCPATH . 'assets/js/script.js') ?>"></script>
  </body>
</html>