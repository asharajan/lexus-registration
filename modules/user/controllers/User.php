<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {


	public function index()
	{
		$this->register();
	}
	
	// a common login function - to be used later
	/* public function login() 
	{
		$this->load->library('user/user_lib');
		$post = array();
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert" style="margin-left: 30%;margin-right: 30%; text-align: center">', '</div>');		
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|trim|valid_email');
		
		if (TRUE == $this->form_validation->run()) {
			$post = $this->input->post();
			$this->security->xss_clean($post);
			if ($user = $this->user_lib->doLogin($post)) {
				$this->session->set_userdata("user_id",$user[0]->user_id);
				$this->session->set_userdata("username",$user[0]->username);
				if ($user[0]->is_admin == 1) {
					$this->session->set_userdata("admin_validated",true);
					redirect('admin/dashboard');
				} else {
					$this->session->set_userdata("validated",true);
					redirect('attendee/displayForm');
				}	
			} else {
				// set flashdata
				$this->session->set_flashdata('flash_message', array(
							'alert_class'	=> 'danger',
							'message'	=> 'Login failed!',
						));
				redirect('user');
			}
		} else {
			$this->load->view('user/login');
		}
	} */
	
	public function logout() 
	{
		$is_admin = $this->session->userdata("admin_validated");
		$user_data = $this->session->all_userdata();

		foreach ($user_data as $key => $value) {
			$this->session->unset_userdata($key);
		}
		
		$this->session->sess_destroy();
		if ($is_admin) {
			redirect('/admin');
		} else {
			redirect('/');
		}
		
	}
	
}
