<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class User_lib {
	
	protected $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		
		$this->CI->load->model('user/user_model');
	}
	
	public function doLogin($args = '')
	{
		if (empty($args))
			return 0;
		
		$args['password'] = SHA1($args['password']);
		$user = $this->CI->user_model->get($args);

		return $user->result();
		
	}
	
	public function get($args = "")
	{
		// get userdata
		$result = $this->CI->user_model->get($args);
		
		return $result->result();
	}
	
	public function doLoginAttendee($args = '')
	{
		if (empty($args))
			return 0;
		
		// if user already exists,do login
		// else add him if password is correct
		$user = $this->CI->user_model->get(array('username'=>$args['username']));
		if ($user->result()[0] != null) {
			return $this->doLogin($args);
		} else {
			if ($args['password'] == "Lexus_register") {
				$args['password'] = SHA1($args['password']);
				$user_id = $this->CI->user_model->insert($args);
				$user = $this->CI->user_model->get(array('user_id'=>$user_id));
				return $user->result();
			}
			return false;
		}
	}
	
	// common login function to be used later
	/*public function doLogin($args= '')
	{
		if (empty($args))
			return 0;
		
		$args['password'] = SHA1($args['password']);
		$user = $this->CI->user_model->get($args);
		
		if ($user->result()[0] != null) {
			return $user->result();
		} else {
			if ($args['password'] == "Lexus_register") {
				$args['password'] = SHA1($args['password']);
				$user_id = $this->CI->user_model->insert($args);
				$user = $this->CI->user_model->get(array('user_id'=>$user_id));
				return $user->result();
			}
			return false;
		}
	}*/
}