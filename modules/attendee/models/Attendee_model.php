<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendee_model extends CI_Model
{
	
	protected $table;

	public function __construct()
	{
		$this->table = $this->db->dbprefix('attendee');
		parent::__construct();
	}
	
	public function insert($args)
	{
		if (empty($args))
			return 0;
		
		$this->db->insert($this->table, $args);
		
		return $this->db->insert_id();
	}
	
	public function get($args='') 
	{
		if (empty($args))
			return false;
		
		$this->db->from($this->table);
		
		if (isset($args) && !empty($args)) 
		{
			foreach($args as $key => $value) 
			{
				$this->db->where($key, $value);
			}
		}
		
		$result = $this->db->get();
		
		return $result;
	}
	
	public function getAttendees($args='') 
	{
		
		$this->db->from($this->table);
		$this->db->select('first_name,last_name,gender,birth_date,nationality,mobile,company,title');
		
		if (isset($args) && !empty($args)) 
		{
			foreach($args as $key => $value) 
			{
				$this->db->where($key, $value);
			}
		}
		
		$result = $this->db->get();
		
		return $result;
	}
	
	public function update($where, $data) {
		if (empty($where))
			return false;
			
		$this->db->where($where);
		return $this->db->update($this->table, $data); 
	}
}

/* End of file User_model.php */
/* Location: ./modules/attendee/models/Attendee_model.php */