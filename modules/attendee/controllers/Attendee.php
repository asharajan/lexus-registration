<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendee extends CI_Controller {

	public function __construct()
    {
        parent::__construct();

        //$this->output->enable_profiler(TRUE);
		$this->load->helper('form');

    }
	
	public function index()
	{
		$this->register();
	}
	
	public function register() {
		// check whether user is validated or not
		$validated = $this->session->userdata("validated");
		
		// if validated, then load the registration form
		if ($validated) 
		{
			$this->displayForm();
		} 
		else 
		{
			$this->load->view("login");
		}
		
	}
	
	public function login()
	{
		$this->load->library('user/user_lib');
		$post = array();
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert" style="margin-left: 30%;margin-right: 30%; text-align: center">', '</div>');		
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|trim|valid_email');
		
		if (TRUE == $this->form_validation->run()) {
			$post = $this->input->post();
			$this->security->xss_clean($post);
			if ($user = $this->user_lib->doLoginAttendee($post)) {
				$this->session->set_userdata("validated",true);
				$this->session->set_userdata("user_id",$user[0]->user_id);
				$this->session->set_userdata("username",$user[0]->username);
				redirect('attendee/displayForm');
			} else {
				// set flashdata
				$this->session->set_flashdata('flash_message', array(
							'alert_class'	=> 'danger',
							'message'	=> 'Login failed!',
						));
				redirect('attendee');
			}
		} else {
			$this->load->view('attendee/login');
		}
	}
	
	
	public function displayForm()
	{
		$data = array();
		$this->load->library('attendee_lib');
		/* $form_template = $this->load->view('attendee-form-'.$page,'',true);
		$data['form'] = $form_template; */
		
		// if user has already registered or is redirected from edit, display his details
		$args = array(
			"user_id" => $this->session->userdata('user_id'),
		);

		if ($this->attendee_lib->hasRegistered($args)) {
			$result = $this->attendee_lib->get($args);
			foreach ($result as $row)
				$data['record'] = $row;
		}

		$this->load->view('attendee-form',$data);
	}
	
	public function edit($guid = '')
	{
		if (date("d-m-Y") >= date("d-m-Y",strtotime("21-03-2016")) ) {
			$this->load->view("attendee/event_expire");
			return;
		}

		$this->load->library('attendee_lib');
		$this->load->library('user/user_lib');
		
		$args['guid'] = urldecode($guid);
		if (($user = $this->attendee_lib->hasRegistered($args)) != null) {
			$this->session->set_userdata('user_id',$user[0]->user_id);
			$user_details = $this->user_lib->get(array("user_id"=>$user[0]->user_id));
			$this->session->set_userdata('username',$user_details[0]->username);
			$this->session->set_userdata('validated',true);
			redirect("attendee/displayForm");
		} else {
			// handle if user not registered
			redirect("attendee/register");
		}
	}
	
	public function is_session_valid() 
	{
		if(!empty($this->session->userdata('user_id'))) {
			echo json_encode(array(
				'response' => true,
			));
		} else {
			$this->session->set_flashdata('flash_message', array(
					'alert_class'	=> 'danger',
					'message'	=> 'Sorry for the inconvenience!!! Session expired!',
				));
			echo json_encode(array(
				'response' => false,
			));
		}
	}
	
	public function submit() 
	{
		$this->load->library('attendee_lib');

		// form validation	
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert" style="margin-left: 30%;margin-right: 30%; text-align: center">', '</div>');		
		
		$this->form_validation->set_rules('first_name', 'First name', 'required|trim');
		$this->form_validation->set_rules('last_name', 'Last name', 'required|trim');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('birth_date', 'Date of birth', 'required');
		$this->form_validation->set_rules('nationality', 'Date of birth', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required');
		$this->form_validation->set_rules('company', 'Company', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('behalf_of', 'Behalf of', 'required');
		$this->form_validation->set_rules('passport_number', 'Passport Number', 'trim');
		$this->form_validation->set_rules('issue_date', 'Issue Date', 'trim');
		$this->form_validation->set_rules('expiry_date', 'Expiry Date', 'trim');
		$this->form_validation->set_rules('require_visa', 'Require Visa', 'trim');
		$this->form_validation->set_rules('arrival_from_airport', 'Arrival from airport', 'trim');
		$this->form_validation->set_rules('arrival_from_date', 'Arrival from date', 'trim');
		$this->form_validation->set_rules('arrival_from_flight', 'Arrival from flight', 'trim');
		$this->form_validation->set_rules('arrival_from_time', 'Arrival from time', 'trim');
		$this->form_validation->set_rules('arrival_to_airport', 'Arrival to airport', 'trim');
		$this->form_validation->set_rules('arrival_to_date', 'Arrival to date', 'trim');
		$this->form_validation->set_rules('arrival_to_flight', 'Arrival to flight', 'trim');
		$this->form_validation->set_rules('arrival_to_time', 'Arrival to time', 'trim');
		$this->form_validation->set_rules('depart_from_airport', 'Depart from airport', 'trim');
		$this->form_validation->set_rules('depart_from_date', 'Depart from date', 'trim');
		$this->form_validation->set_rules('depart_from_flight', 'Depart from flight', 'trim');
		$this->form_validation->set_rules('depart_from_time', 'Depart from time', 'trim');
		$this->form_validation->set_rules('depart_to_airport', 'Depart to airport', 'trim');
		$this->form_validation->set_rules('depart_to_date', 'Depart to date', 'trim');
		$this->form_validation->set_rules('depart_to_flight', 'Depart to flight', 'trim');
		$this->form_validation->set_rules('depart_to_time', 'Depart to time', 'trim');
		$this->form_validation->set_rules('arrival_pickup', 'Arrival Pickup', 'trim');
		$this->form_validation->set_rules('airport_drop', 'Airport drop off', 'trim');
		$this->form_validation->set_rules('early_check_in', 'Early Check in', 'trim');
		$this->form_validation->set_rules('check_in_time', 'Early Check in', 'trim');
		$this->form_validation->set_rules('late_check_out', 'Late Check out', 'trim');
		$this->form_validation->set_rules('check_out_time', 'Late Check out', 'trim');
		$this->form_validation->set_rules('room_prefer', 'Room Preference', 'trim');
		$this->form_validation->set_rules('allergy', 'Allergy', 'trim');
		$this->form_validation->set_rules('diet', 'Dietary Preference', 'trim');
		$this->form_validation->set_rules('diet_extra', 'Dietary Preference', 'trim');
		$this->form_validation->set_rules('main_course', 'Main Course', 'trim');
		$this->form_validation->set_rules('main_course_extra', 'Main Course', 'trim');
		$this->form_validation->set_rules('ap_time', 'AP Forum', 'required');
		$this->form_validation->set_rules('dinner_time', 'Cocktail + Dinner', 'required');
		$this->form_validation->set_rules('observation_time', 'Showroom Observation', 'required');
		$this->form_validation->set_rules('showroom_transport', 'Showroom Transport', 'required');
		$this->form_validation->set_rules('airport_transport', 'Airport Transport', 'required');
		$this->form_validation->set_rules('showroom_hotel', 'Hotel Transport', 'required');

		if (TRUE == $this->form_validation->run()) {
			$post = $this->input->post();
			$saved = $this->attendee_lib->saveSession($post);
			$result = $this->attendee_lib->insert($post);
			if ($result) {
				$this->load->view('thankyou');
			} else {
				$this->session->set_flashdata('flash_message', array(
						'alert_class'	=> 'danger',
						'message'	=> 'Sorry for the inconvenience!!! Failed to register!',
					));
				$this->register();
			}
		} else {
			//handle form validation fail
			//redirect('attendee/displayForm');
			//print_r($this->input->post());
			//echo validation_errors();
		}
	}
	
	/*public function submit1($form = 1) 
	{
		$this->load->library('attendee_lib');
		$post = $this->input->post();
		// form validation	
		switch($form) {
			case 1: $this->form_validation->set_rules('first_name', 'First name', 'required|trim');
					$this->form_validation->set_rules('last_name', 'Last name', 'required|trim');
					$this->form_validation->set_rules('gender', 'Gender', 'required');
					$this->form_validation->set_rules('birth_date', 'Date of birth', 'required');
					$this->form_validation->set_rules('nationality', 'Date of birth', 'required');
					$this->form_validation->set_rules('mobile', 'Mobile', 'required');
					$this->form_validation->set_rules('company', 'Company', 'required');
					$this->form_validation->set_rules('title', 'Title', 'required');
			break;
			case 2: $this->form_validation->set_rules('passport_number', 'Passport Number', 'required|trim');
					$this->form_validation->set_rules('issue_date', 'Issue Date', 'required|trim');
					$this->form_validation->set_rules('expiry_date', 'Expiry Date', 'required');
					$this->form_validation->set_rules('require_visa', 'Require Visa', 'required');
					$this->form_validation->set_rules('arrival_from_airport', 'Arrival from airport', 'required');
					$this->form_validation->set_rules('arrival_from_date', 'Arrival from date', 'required');
					$this->form_validation->set_rules('arrival_from_flight', 'Arrival from flight', 'required');
					$this->form_validation->set_rules('arrival_from_hour', 'Arrival from hour', 'required');
					$this->form_validation->set_rules('arrival_from_min', 'Arrival from minutes', 'required');
					$this->form_validation->set_rules('arrival_to_airport', 'Arrival to airport', 'required');
					$this->form_validation->set_rules('arrival_to_date', 'Arrival to date', 'required');
					$this->form_validation->set_rules('arrival_to_flight', 'Arrival to flight', 'required');
					$this->form_validation->set_rules('arrival_to_hour', 'Arrival to hour', 'required');
					$this->form_validation->set_rules('arrival_to_min', 'Arrival to minutes', 'required');
					$this->form_validation->set_rules('depart_from_airport', 'Depart from airport', 'required');
					$this->form_validation->set_rules('depart_from_date', 'Depart from date', 'required');
					$this->form_validation->set_rules('depart_from_flight', 'Depart from flight', 'required');
					$this->form_validation->set_rules('depart_from_hour', 'Depart from hour', 'required');
					$this->form_validation->set_rules('depart_from_min', 'Depart from minutes', 'required');
					$this->form_validation->set_rules('depart_to_airport', 'Depart to airport', 'required');
					$this->form_validation->set_rules('depart_to_date', 'Depart to date', 'required');
					$this->form_validation->set_rules('depart_to_flight', 'Depart to flight', 'required');
					$this->form_validation->set_rules('depart_to_hour', 'Depart to hour', 'required');
					$this->form_validation->set_rules('depart_to_min', 'Depart to minutes', 'required');
					$this->form_validation->set_rules('arrival_pickup', 'Arrival Pickup', 'required');
					$this->form_validation->set_rules('airport_drop', 'Airport drop off', 'required');
			break;
			case 3: $this->form_validation->set_rules('early_check_in', 'Early Check in', 'required');
					$this->form_validation->set_rules('check_in_hour', 'Early Check in', 'trim');
					$this->form_validation->set_rules('check_in_time', 'Early Check in', 'trim');
					$this->form_validation->set_rules('check_in_min', 'Early Check in', 'trim');
					$this->form_validation->set_rules('late_check_out', 'Late Check out', 'required');
					$this->form_validation->set_rules('check_in_hour', 'Late Check out', 'trim');
					$this->form_validation->set_rules('check_in_time', 'Late Check out', 'trim');
					$this->form_validation->set_rules('check_in_min', 'Late Check out', 'trim');
					$this->form_validation->set_rules('room_prefer', 'Room Preference', 'trim');
					$this->form_validation->set_rules('allergy', 'Allergy', 'trim');
					$this->form_validation->set_rules('diet', 'Dietary Preference', 'trim');
					$this->form_validation->set_rules('main_course', 'Main Course', 'required');
					
			break;
			case 4: $this->form_validation->set_rules('ap_time', 'AP Forum', 'required');
					$this->form_validation->set_rules('dinner_time', 'Cocktail + Dinner', 'required');
					$this->form_validation->set_rules('observation_time', 'Showroom Observation', 'required');
					$this->form_validation->set_rules('showroom_transport', 'Showroom Transport', 'required');
					$this->form_validation->set_rules('airport_transport', 'Airport Transport', 'required');
			break;
		}
		
		if (TRUE == $this->form_validation->run()) {
			$saved = $this->attendee_lib->saveSession($post);
			if ($form == 4){
				$result = $this->attendee_lib->insert();
				if ($result) {
					$this->load->view('thankyou');
				}
			} else if ($saved){
				$this->displayForm($form+1);
			}
		} else {
			$this->displayForm($form);
		}
	}*/
}
