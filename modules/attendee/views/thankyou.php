<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php 

// Load header
$this->load->view('header');?>
    
    <div class="main-content">
      <div class="main-container container">
        <div class="thank-you text-center">
			<?php $flash_message = $this->session->flashdata('flash_message');
			if (isset($flash_message) AND !empty($flash_message)) { ?>
			<div class="alert <?php echo isset($flash_message['alert_class']) ? "alert-{$flash_message['alert_class']}":'' ?> text-center">
				<p style="text-align:center"><?php echo isset($flash_message['message']) ? $flash_message['message']:'Something went wrong. Please refresh the page.'; ?></p>
			</div>
			<?php } else { ?>
			<h2>Your registration has been successfully submitted.<br/>Full event information will be sent to your email inbox one week prior to the event.</h2>
			<?php }?>
        </div>
      </div>
    </div>
    <!-- / .main-content -->
    
<?php   
// Load footer   
 $this->load->view('footer');