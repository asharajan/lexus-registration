<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php 

$user_id = $this->session->userdata('user_id');
if (!isset($user_id) && empty($user_id))
	redirect('attendee');
// Load header
$this->load->view('header');?>
    
    <div class="main-content">
      <div class="container">
        <h1> Lexus Asia Pacific Online Registration</h1>
        
        <?php //echo $form;?>
		<div>
		<!-- Show success or error messages here -->
		<?php echo validation_errors();?>
		<?php $flash_message = $this->session->flashdata('flash_message');
		if (isset($flash_message) AND !empty($flash_message)) : ?>
		<div class="alert <?php echo isset($flash_message['alert_class']) ? "alert-{$flash_message['alert_class']}":'' ?> text-center">
			<p style="text-align:center"><?php echo isset($flash_message['message']) ? $flash_message['message']:'Something went wrong. Please refresh the page.'; ?></p>
		</div>
		<?php endif; ?>
		<!-- END Show success or error messages here -->
		<form id="registration_form" method="post" action="<?php echo base_url('attendee/submit');?>" >
		  <!-- Nav tabs -->
		  <ul class="nav nav-tabs" id="steps" role="tablist" style="display:none;">
			<li role="presentation" class="active"><a href="#form_1" aria-controls="form_1" role="tab" data-toggle="tab">Step 1</a></li>
			<li role="presentation"><a href="#form_2" aria-controls="form_2" role="tab" data-toggle="tab">Step 2</a></li>
			<li role="presentation"><a href="#form_3" aria-controls="form_3" role="tab" data-toggle="tab">Step 3</a></li>
			<li role="presentation"><a href="#form_4" aria-controls="form_4" role="tab" data-toggle="tab">Step 4</a></li>
			<li role="presentation"><a href="#form_5" aria-controls="form_5" role="tab" data-toggle="tab">Step 5</a></li>
			<li role="presentation"><a href="#summary" aria-controls="summary" role="tab" data-toggle="tab">Summary</a></li>
		  </ul>
		  
		  <!-- Tab panes -->
		  <div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="form_1">
				 <div class="reg-form">
				  <div class="row">
					<div class="col-md-8">
					  
					  <div class="form-group">
						<p class="text-danger">* Required</p>
					  </div>
						
					  <div class="form-group">
						<h2>Personal Details</h2>
					  </div>
					
					  <div class="form-group">
						<label class="control-label">First Name <span class="text-danger">*</span></label>
						<input type="text" class="form-control required_1" name="first_name" placeholder="First Name" value="<?php echo isset($record->first_name) ? $record->first_name:"";?>">
						<p class="help-block"><span class="text-danger">First name is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Last Name <span class="text-danger">*</span></label>
						<input type="text" class="form-control required_1" name="last_name" placeholder="Last Name" value="<?php echo isset($record->last_name) ? $record->last_name:"";?>">
						<p class="help-block"><span class="text-danger">Last name is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Gender <span class="text-danger">*</span></label><br/>
						  <label class="radio-inline">
							<input type="radio" name="gender" value="F" <?php echo isset($record->gender) ? ($record->gender == 'F' ? "checked":""):"checked";?>>
							Female
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="gender" value="M" <?php echo isset($record->gender) ? ($record->gender == 'M' ? "checked":""):"";?>>
							Male
						  </label>
						<p class="help-block"><span class="text-danger">Gender is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Date of Birth <span class="text-danger">*</span></label>
						<input type="text" class="form-control required_1" name="birth_date" placeholder="dd/mm/yyyy" value="<?php echo isset($record->birth_date) ? date('d/m/Y', strtotime($record->birth_date)):"";?>">
						<p class="help-block"><span class="text-danger">Date of Birth is required.</span></p>
						<p class="help-block incorrect"><span class="text-danger">Please enter in the format dd/mm/yyyy.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Nationality <span class="text-danger">*</span></label>
						<input type="text" class="form-control required_1" name="nationality" placeholder="Nationality" value="<?php echo isset($record->nationality) ? $record->nationality:"";?>">
						<p class="help-block"><span class="text-danger">Nationality is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Mobile Number <span class="text-danger">*</span></label>
						<input type="text" class="form-control required_1" name="mobile" placeholder="+Country Code &#45; Mobile number" value="<?php echo isset($record->mobile) ? $record->mobile:"";?>">
						<p class="help-block"><span class="text-danger">Mobile Number is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Company <span class="text-danger">*</span></label>
						<input type="text" class="form-control required_1" name="company" placeholder="Company" value="<?php echo isset($record->company) ? $record->company:"";?>">
						<p class="help-block"><span class="text-danger">Company is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Title <span class="text-danger">*</span></label>
						<input type="text" class="form-control required_1" name="title" placeholder="Title" value="<?php echo isset($record->title) ? $record->title:"";?>">
						<p class="help-block"><span class="text-danger">Title is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Are you filling up the form on behalf of someone? <span class="text-danger">*</span></label><br/>
						  <label class="radio-inline">
							<input type="radio" name="behalf_of" value="Yes" <?php echo isset($record->behalf_of) ? ($record->behalf_of == 'Yes' ? "checked":""):"";?>>
							Yes
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="behalf_of" value="No" <?php echo isset($record->behalf_of) ? ($record->behalf_of == 'No' ? "checked":""):"checked";?>>
							No
						  </label>
						<p class="help-block"><span class="text-danger">This field is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<ul class="list-inline">
						  <li>
							<a class="btn btn-primary" id="next1">Next</a>
						  </li>
						  <li>
							<a class="btn btn-success" name="save_1">Save</a>
						  </li>
						</ul>
					  </div>
					  
					  <div class="form-group">
						<div class="progress">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
							20%
						  </div>
						</div>
					  </div>
						
					</div>
				  </div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="form_2">
				<div class="reg-form">
				  <div class="row">
					<div class="col-md-8">
					  
					  <div class="form-group">
						<p class="text-danger">* Required</p>
					  </div>
					  
					  <div class="form-group">
						<h2>Travel Details</h2>
					  </div>
				
					  <div class="form-group">
						<label class="control-label">Passport Number</label>
						<input type="text" class="form-control" name="passport_number" placeholder="Passport Number" value="<?php echo isset($record->passport_number) && !empty($record->passport_number) ? $record->passport_number:"";?>">
						<p class="help-block"><span class="text-danger">Passport Number is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Passport Issue Date</label>
						<input type="text" class="form-control" name="issue_date" placeholder="dd/mm/yyyy" value="<?php echo isset($record->issue_date) && !empty($record->issue_date)? date('d/m/Y', strtotime($record->issue_date)):"";?>">
						<p class="help-block"><span class="text-danger">Issue Date is required.</span></p>
						<p class="help-block incorrect"><span class="text-danger">Please enter in the format dd/mm/yyyy.</span></p>
						<p class="help-block incorrect_date"><span class="text-danger">Issue Date should be earlier than expiry date</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Passport Expiry Date</label>
						<input type="text" class="form-control" name="expiry_date" placeholder="dd/mm/yyyy" value="<?php echo isset($record->expiry_date) && !empty($record->expiry_date) ? date('d/m/Y', strtotime($record->expiry_date)):"";?>">
						<p class="help-block"><span class="text-danger">Expiry Date is required.</span></p>
						<p class="help-block incorrect"><span class="text-danger">Please enter in the format dd/mm/yyyy.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Do you require a Visa?</label><br/>
						  <label class="radio-inline">
							<input type="radio" name="require_visa" value="Yes" <?php echo isset($record->require_visa) && !empty($record->require_visa)? ($record->require_visa == "Yes" ? "checked":""):"";?>>
							Yes
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="require_visa" value="No" <?php echo isset($record->require_visa) && !empty($record->require_visa) ? ($record->require_visa == "No" ? "checked":""):"";?>>
							No
						  </label>
						<p class="help-block"><span class="text-danger">Visa Information required</span></p>
					  </div>
					  
					  <div class="form-group">
						<h2>In-bound Flight</h2>
						<h3>Departure (From)</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Country-Airport Name</label>
						<input type="text" class="form-control" name="arrival_from_airport" placeholder="e.g. Japan-NRT" value="<?php echo isset($record->arrival_from_airport) && !empty($record->arrival_from_airport)? $record->arrival_from_airport:"";?>">
						<p class="help-block"><span class="text-danger">Airport is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Date</label>
						<input type="text" class="form-control datepicker" name="arrival_from_date" placeholder="dd/mm/yyyy" value="<?php echo isset($record->arrival_from_date) && !empty($record->arrival_from_date) ? date('d/m/Y', strtotime($record->arrival_from_date)):"";?>" readonly>
						<p class="help-block"><span class="text-danger">Date is required.</span></p>
						<p class="help-block incorrect_date"><span class="text-danger">Departure date should be earlier than Arrival date.</span></p>
					  </div>
					  
					  <div class="form-group bootstrap-timepicker">
						<label class="control-label">Time </label>
						<input type="text" class="form-control timepicker" name="arrival_from_time" value="<?php echo isset($record->arrival_from_time) && !empty($record->arrival_from_time)? $record->arrival_from_time:"";?>">
						<p class="help-block"><span class="text-danger">Time of Departure is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Flight Number </label>
						<input type="text" class="form-control" name="arrival_from_flight" placeholder="Flight number" value="<?php echo isset($record->arrival_from_flight) && !empty($record->arrival_from_flight)? $record->arrival_from_flight:"";?>">
						<p class="help-block"><span class="text-danger">Flight number is required.</span></p>
					  </div>

					  <div class="form-group">
						<h3>Arrival (To)</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Country-Airport Name</label>
						<input type="text" class="form-control" name="arrival_to_airport" placeholder="e.g. Japan-NRT" value="<?php echo isset($record->arrival_to_airport) && !empty($record->arrival_to_airport)? $record->arrival_to_airport:"";?>">
						<p class="help-block"><span class="text-danger">Airport is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Date</label>
						<input type="text" class="form-control datepicker" name="arrival_to_date" placeholder="dd/mm/yyyy" value="<?php echo isset($record->arrival_to_date) && !empty($record->arrival_to_date) ? date('d/m/Y', strtotime($record->arrival_to_date)):"";?>" readonly>
						<p class="help-block"><span class="text-danger">Date of Arrival is required.</span></p>
					  </div>
					  
					  <div class="form-group bootstrap-timepicker">
						<label class="control-label">Time </label>
							<input type="text" class="form-control timepicker" name="arrival_to_time" value="<?php echo isset($record->arrival_to_time) && !empty($record->arrival_to_time)? $record->arrival_to_time:"";?>">
							<p class="help-block"><span class="text-danger">Time of Arrival is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Flight Number</label>
						<input type="text" class="form-control" name="arrival_to_flight" placeholder="Flight Number" value="<?php echo isset($record->arrival_to_flight) && !empty($record->arrival_to_flight)? $record->arrival_to_flight:"";?>">
						<p class="help-block"><span class="text-danger">Flight Number is required.</span></p>
					  </div>

					  <div class="form-group">
						<label class="control-label">Pickup at airport? </label><br/>
						  <label class="radio-inline">
							<input type="radio" name="arrival_pickup" value="Yes" <?php echo isset($record->arrival_pickup) && !empty($record->arrival_pickup)? ($record->arrival_pickup == "Yes" ? "checked":""):"";?>>
							Yes
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="arrival_pickup" value="No" <?php echo isset($record->arrival_pickup) && !empty($record->arrival_pickup) ? ($record->arrival_pickup == "No" ? "checked":""):"";?>>
							No
						  </label>
						<p class="help-block"><span class="text-danger">Pickup at airport?</span></p>
					  </div>

					  <div class="form-group">
						<ul class="list-inline">
						  <li>
							<a class="btn btn-default" id="back1">Back</a>
						  </li>
						  <li>
							<a class="btn btn-primary" id="next2">Next</a>
						  </li>
						  <li>
							<a class="btn btn-success" name="save_2">Save</a>
						  </li>
						</ul>
					  </div>
					  
					  <div class="form-group">
						<div class="progress">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%;">
							40%
						  </div>
						</div>
					  </div>
					
					</div>
				  </div>
				</div>
			</div>
			
			<!-- new -->
			<div role="tabpanel" class="tab-pane" id="form_3">
				 <div class="reg-form">
				  <div class="row">
					<div class="col-md-8">
					  
					  <div class="form-group">
						<p class="text-danger">* Required</p>
					  </div>
						
					  <div class="form-group">
						<h2>Out-bound Flight</h2>
						<h3>Departure (From)</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Country-Airport Name</label>
						<input type="text" class="form-control" name="depart_from_airport" placeholder="e.g. Japan-NRT" value="<?php echo isset($record->depart_from_airport) && !empty($record->depart_from_airport)? $record->depart_from_airport:"";?>">
						<p class="help-block"><span class="text-danger">Airport is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Date</label>
						<input type="text" class="form-control datepicker" name="depart_from_date" placeholder="dd/mm/yyyy" value="<?php echo isset($record->depart_from_date) && !empty($record->depart_from_date)? date('d/m/Y', strtotime($record->depart_from_date)):"";?>" readonly>
						<p class="help-block"><span class="text-danger">Date is required.</span></p>
						<p class="help-block incorrect_date"><span class="text-danger">Departure date should be earlier than Arrival date.</span></p>
					  </div>
					  
					  <div class="form-group bootstrap-timepicker">
						<label class="control-label">Time</label>
							<input type="text" class="form-control timepicker" name="depart_from_time" value="<?php echo isset($record->depart_from_time) && !empty($record->depart_from_time) ? $record->depart_from_time:"";?>">
							<p class="help-block"><span class="text-danger">Time of Departure is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Flight Number </label>
						<input type="text" class="form-control" name="depart_from_flight" placeholder="Flight Number" value="<?php echo isset($record->depart_from_flight) && !empty($record->depart_from_flight) ? $record->depart_from_flight:"";?>">
						<p class="help-block"><span class="text-danger">Flight Number is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Send off to airport? </label><br/>
						  <label class="radio-inline">
							<input type="radio" name="airport_drop" value="Yes" <?php echo isset($record->airport_drop) && !empty($record->airport_drop)? ($record->airport_drop == "Yes" ? "checked":""):"";?>>
							Yes
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="airport_drop" value="No" <?php echo isset($record->airport_drop) && !empty($record->airport_drop) ? ($record->airport_drop == "No" ? "checked":""):"";?>>
							No
						  </label>
						<p class="help-block"><span class="text-danger">Send off to airport?</span></p>
					  </div>
					  
					  <div class="form-group">
						<h3>Arrival (To)</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Country-Airport Name</label>
						<input type="text" class="form-control" name="depart_to_airport" placeholder="e.g. Japan-NRT" value="<?php echo isset($record->depart_to_airport) && !empty($record->depart_to_airport) ? $record->depart_to_airport:"";?>">
						<p class="help-block"><span class="text-danger">Airport is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Date </label>
						<input type="text" class="form-control datepicker" name="depart_to_date" placeholder="dd/mm/yyyy" value="<?php echo isset($record->depart_to_date) && !empty($record->depart_to_date)? date('d/m/Y', strtotime($record->depart_to_date)):"";?>" readonly>
						<p class="help-block"><span class="text-danger">Date is required.</span></p>
					  </div>
					  
					  <div class="form-group bootstrap-timepicker">
						<label class="control-label">Time</label>
							<input type="text" class="form-control timepicker" name="depart_to_time" value="<?php echo isset($record->depart_to_time) && !empty($record->depart_to_time)? $record->depart_to_time:"";?>">
							<p class="help-block"><span class="text-danger">Time of Arrival is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Flight Number </label>
						<input type="text" class="form-control" name="depart_to_flight" placeholder="Flight Number" value="<?php echo isset($record->depart_to_flight) && !empty($record->depart_to_flight) ? $record->depart_to_flight:"";?>">
						<p class="help-block"><span class="text-danger">Flight Number is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<ul class="list-inline">
						  <li>
							<a class="btn btn-default" id="back2">Back</a>
						  </li>
						  <li>
							<a class="btn btn-primary" id="next3">Next</a>
						  </li>
						  <li>
							<a class="btn btn-success" name="save_3">Save</a>
						  </li>
						</ul>
					  </div>
					  
					  <div class="form-group">
						<div class="progress">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
							60%
						  </div>
						</div>
					  </div>
						
					</div>
				  </div>
				</div>
			</div>
			
			<div role="tabpanel" class="tab-pane" id="form_4">
				<div class="reg-form">
				  <div class="row">
					<div class="col-md-8">
					  
					  <div class="form-group">
						<p class="text-danger">* Required</p>
					  </div>
					  
					  <div class="form-group">
						<h2>Hotel Details</h2>
					  </div>
				
					  <div class="form-group">
						<label class="control-label">Date of check-in </label>
						<input type="text" class="form-control datepicker" name="check_in_date" placeholder="dd/mm/yyyy" value="<?php echo isset($record->check_in_date) && !empty($record->check_in_date) ? date('d/m/Y', strtotime($record->check_in_date)):"";?>" readonly>
						<p class="help-block"><span class="text-danger">Date of check-in is required.</span></p>
						<p class="help-block incorrect_date"><span class="text-danger">Check In Date should be earlier than Check Out date</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Date of check-out</label>
						<input type="text" class="form-control datepicker" name="check_out_date" placeholder="dd/mm/yyyy" value="<?php echo isset($record->check_out_date) && !empty($record->check_out_date) ? date('d/m/Y', strtotime($record->check_out_date)):"";?>" readonly>
						<p class="help-block"><span class="text-danger">Date of check-out  is required.</span></p>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Early Check-In required?</label><br/>
							<label class="radio-inline">
								<input type="radio" name="early_check_in" value="Yes" <?php echo isset($record->early_check_in) && !empty($record->early_check_in) ? ($record->early_check_in == "Yes" ? "checked":""):"";?> data-show-field="check_in_time">Yes
							</label>
							<label class="radio-inline">
								<input type="radio" name="early_check_in" value="No" <?php echo isset($record->early_check_in) && !empty($record->early_check_in)? ($record->early_check_in == "No" ? "checked":""):"";?> data-hide-field="check_in_time">No
							</label>
							<p class="help-block"><span class="text-danger">Early Check-In required?</span></p>
					  </div>
					  
					  <div class="form-group bootstrap-timepicker hidden">
						<ul class="list-inline">
						  <li>
							<label class="control-label">Early Check-In time:</label>
						  </li>
						  <li>
							<input type="text" class="form-control timepicker" name="check_in_time" value="<?php echo isset($record->check_in_time) && !empty($record->check_in_time)? $record->check_in_time:"";?>">
						  </li>
						  <!--<li>
							<input type="number" class="form-control" name="check_in_min" value="" min="1" max="59">
						  </li>
						  <li>
							<select class="form-control" name="check_in_time">
							  <option value="am">AM</option>
							  <option value="pm">PM</option>
							</select>
						  </li>-->
						</ul>
						<p class="help-block"><span class="text-danger">If yes, please state the time.</span></p>
					  </div>
					  
					  
					  <div class="form-group">
						<label class="control-label">Late Check-Out required?</label><br/>
						  <label class="radio-inline">
							<input type="radio" name="late_check_out" value="Yes" <?php echo isset($record->late_check_out) && !empty($record->late_check_out)? ($record->late_check_out == "Yes" ? "checked":""):"";?> data-show-field="check_out_time">
							Yes
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="late_check_out" value="No" <?php echo isset($record->late_check_out) && !empty($record->late_check_out)? ($record->late_check_out == "No" ? "checked":""):"";?> data-hide-field="check_out_time">
							No
						  </label>
						<p class="help-block"><span class="text-danger">Late Check-Out required? </span></p>
					  </div>
					  
					  <div class="form-group bootstrap-timepicker hidden">
						<ul class="list-inline">
						  <li>
							<label class="control-label">Late Check-Out time:</label>
						  </li>
						  <li>
							<input type="text" class="form-control timepicker" name="check_out_time" value="<?php echo isset($record->check_out_time) && !empty($record->check_out_time)? $record->check_out_time:"";?>">
						  </li>
						  <!--<li>
							<input type="number" class="form-control" name="check_out_min" value="" min="1" max="59">
						  </li>
						  <li>
							<select class="form-control" name="check_out_time">
							  <option value="am">AM</option>
							  <option value="pm">PM</option>
							</select>
						  </li>-->
						</ul>
					   </div>
					   
					   <div class="form-group">
							<p><i>Note: Early Check-In and/or Late Check-Out may incur additional charges.</i></p>
					   </div>
					  
					  <div class="form-group">
						<h2>Additional Information</h2>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Room Preference</label><br/>
						  <label class="radio-inline">
							<input type="radio" name="room_prefer" value="Smoking" <?php echo isset($record->room) && !empty($record->room)? ($record->room == "Smoking" ? "checked":""):"";?>>
							Smoking
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="room_prefer" value="Non-smoking" <?php echo isset($record->room) && !empty($record->room)? ($record->room == "Non-smoking" ? "checked":""):"";?>>
							Non-smoking
						  </label>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Allergy</label>
						<input type="text" class="form-control" name="allergy" placeholder="" value="<?php echo isset($record->allergy) && !empty($record->allergy)? $record->allergy:"";?>">
					  </div>
					  
					  <div class="form-group">
						<label class="control-label" <?php echo isset($record->diet) ? $record->diet:"";?>>Dietary Preference (You may select more than one.)</label><br/>
						<label class="checkbox-inline"><input type="checkbox" name="diet[]" value="No Beef" <?php echo isset($record->diet) && !empty($record->diet) ? (strpos($record->diet,"No Beef") !== FALSE ? "checked":""):"";?>>No Beef</label>
						<label class="checkbox-inline"><input type="checkbox" name="diet[]" value="No Pork" <?php echo isset($record->diet) && !empty($record->diet) ? (strpos($record->diet,"No Pork") !== FALSE ? "checked":""):"";?>>No Pork</label>
						<label class="checkbox-inline"><input type="checkbox" name="diet[]" value="No Mutton" <?php echo isset($record->diet) && !empty($record->diet)? (strpos($record->diet,"No Mutton") !== FALSE ? "checked":""):"";?>>No Mutton</label>
						<label class="checkbox-inline"><input type="checkbox" name="diet[]" value="No Shell Fish, Fish is OK" <?php echo isset($record->diet) && !empty($record->diet)? (strpos($record->diet,"No Shell Fish, Fish is OK") !== FALSE ? "checked":""):"";?>>No Shell Fish, Fish is OK</label>
						<label class="checkbox-inline"><input type="checkbox" name="diet[]" value="Vegetarian" <?php echo isset($record->diet) && !empty($record->diet)? (strpos($record->diet,"Vegetarian") !== FALSE ? "checked":""):"";?>>Vegetarian</label>
						<input type="hidden" value="" name="diet">
					  </div>
					
					  <div class="form-group">
						<!--<input type="text" class="form-control" name="diet" placeholder="Dietary Preference" value="<?php //echo isset($record->diet) ? $record->diet:"";?>">-->
						<label>
							<input type="text" class="form-control col-md-4" name="diet_extra" placeholder="Others, please specify" value="<?php echo isset($record->diet_extra) && !empty($record->diet_extra) ? $record->diet_extra:"";?>">
						</label>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">Main Course Preference (Select ONE)</label>
						<select name="main_course" class="form-control" >
							<option value="" <?php echo isset($record->main_course) && !empty($record->main_course) ? ($record->main_course == "" ? "selected":""):"";?>>Select an option</option>
							<option value="Beef" <?php echo isset($record->main_course) && !empty($record->main_course)? ($record->main_course == "Beef" ? "selected":""):"";?>>Beef</option>
							<option value="Chicken" <?php echo isset($record->main_course) && !empty($record->main_course)? ($record->main_course == "Chicken" ? "selected":""):"";?>>Chicken</option>
							<option value="Fish" <?php echo isset($record->main_course) && !empty($record->main_course)? ($record->main_course == "Fish" ? "selected":""):"";?>>Fish</option>
							<option value="Vegetarian" <?php echo isset($record->main_course) && !empty($record->main_course) ? ($record->main_course == "Vegetarian" ? "selected":""):"";?>>Vegetarian</option>
							<option value="Others" <?php echo isset($record->main_course) && !empty($record->main_course)? ($record->main_course == "Others" ? "selected":""):"";?>>Others</option>
						</select>
						<p class="help-block"><span class="text-danger">Please select one.</span></p>
					  </div>
					
					  <div class="form-group">
						<label>
							<input type="text" class="form-control col-md-4 <?php echo isset($record->main_course) ? ($record->main_course == "Others" ? "":"hidden"):"hidden";?>" name="main_course_extra" placeholder="Others, please specify" value="<?php echo isset($record->main_course_extra) && !empty($record->main_course_extra) ? $record->main_course_extra:"";?>">
						</label>
					  </div>
					  
					  <div class="form-group">
						<ul class="list-inline">
						  <li>
							<a id="back3" class="btn btn-default">Back</a>
						  </li>
						  <li>
							<a class="btn btn-primary" id="next4">Next</a>
						  </li>
						  <li>
							<a class="btn btn-success" name="save_4">Save</a>
						  </li>
						</ul>
					  </div>
					  
					  <div class="form-group">
						<div class="progress">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
							80%
						  </div>
						</div>
					  </div>
					
					</div>
				  </div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="form_5">
				<div class="reg-form">
				  <div class="row">
					<div class="col-md-8">
					  
					  <div class="form-group">
						<p class="text-danger">* Required</p>
					  </div>
				
					  <div class="form-group">
						<h2>Participation Details</h2>
						<p>Reserve your seat at the following activities by clicking “Yes” for  those you will be attending.</p>
					  </div>
					  
					  <div class="form-group">
						<h3>Lexus AP Forum</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">20 Apr 2016 (Wed) &#64; 09:00 &#45; 17:00 <span class="text-danger">*</span></label>
						  <div>
						  <label class="radio-inline">
							<input type="radio" name="ap_time" value="Yes" <?php echo isset($record->ap_forum)  && !empty($record->ap_forum)? ($record->ap_forum == "Yes" ? "checked":""):"checked";?>>
							Yes
						  </label>
						  <label class="radio-inline">
							<input type="radio" name="ap_time" value="No" <?php echo isset($record->ap_forum)  && !empty($record->ap_forum)? ($record->ap_forum == "No" ? "checked":""):"";?>>
							No
						  </label>
							<p class="help-block"><span class="text-danger">Please select.</span></p>
						  </div>
					  </div>
					  
					  <div class="form-group">
						<h3>Cocktail + Official Dinner</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">20 Apr 2016 (Wed) &#64; 18:30 &#45; 21:00 <span class="text-danger">*</span></label>
						  <div>
							  <label class="radio-inline">
								<input type="radio" name="dinner_time" value="Yes" <?php echo isset($record->cocktail_dinner)  && !empty($record->cocktail_dinner)? ($record->cocktail_dinner == "Yes" ? "checked":""):"checked";?>>
								Yes
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="dinner_time" value="No" <?php echo isset($record->cocktail_dinner)  && !empty($record->cocktail_dinner)? ($record->cocktail_dinner == "No" ? "checked":""):"";?>>
								No
							  </label>
							<p class="help-block"><span class="text-danger">Please select.</span></p>
						  </div>
					  </div>
					  
					  <div class="form-group">
						<h3>Transport from Hotel to Showroom</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">21 Apr 2016 (Thu) &#64; 13:30 &#45; 14:00 <span class="text-danger">*</span></label>
						  <div>
							  <label class="radio-inline">
								<input type="radio" name="showroom_transport" value="Yes" <?php echo isset($record->hotel_showroom) && !empty($record->hotel_showroom) ? ($record->hotel_showroom == "Yes" ? "checked":""):"checked";?>>
								Yes
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="showroom_transport" value="No" <?php echo isset($record->hotel_showroom) && !empty($record->hotel_showroom) ? ($record->hotel_showroom == "No" ? "checked":""):"";?>>
								No
							  </label>
							  <p class="help-block"><span class="text-danger">Please select.</span></p>
						  </div>
					  </div>
					  
					  <div class="form-group">
						<h3>Lexus Showroom Observation</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">21 Apr 2016 (Thu) &#64; 14:00 &#45; 15:00 <span class="text-danger">*</span></label>
						  <div>
							  <label class="radio-inline">
								<input type="radio" name="observation_time" value="Yes" <?php echo isset($record->showroom_observe) && !empty($record->showroom_observe)? ($record->showroom_observe == "Yes" ? "checked":""):"checked";?>>
								Yes
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="observation_time" value="No" <?php echo isset($record->showroom_observe) && !empty($record->showroom_observe)? ($record->showroom_observe == "No" ? "checked":""):"";?>>
								No
							  </label>
							  <p class="help-block"><span class="text-danger">Please select.</span></p>
						  </div>
					  </div>
					  
					  <div class="form-group">
						<h3>Transport from Showroom to Airport</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">21 Apr 2016 (Thu) &#64; 15:00 &#45; 16:00 <span class="text-danger">*</span></label>
						  <div>
							  <label class="radio-inline">
								<input type="radio" name="airport_transport" value="Yes" <?php echo isset($record->showroom_airport) && !empty($record->showroom_airport) ? ($record->showroom_airport == "Yes" ? "checked":""):"checked";?>>
								Yes
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="airport_transport" value="No" <?php echo isset($record->showroom_airport) && !empty($record->showroom_airport) ? ($record->showroom_airport == "No" ? "checked":""):"";?>>
								No
							  </label>
							  <p class="help-block"><span class="text-danger">Please select.</span></p>
						  </div>
					  </div>
					  
					  <div class="form-group">
						<h3>Transport from Showroom to Hotel</h3>
					  </div>
					  
					  <div class="form-group">
						<label class="control-label">21 Apr 2016 (Thu) &#64; 15:00 &#45; 15:30 <span class="text-danger">*</span></label>
						  <div>
							  <label class="radio-inline">
								<input type="radio" name="showroom_hotel" value="Yes" <?php echo isset($record->showroom_hotel) && !empty($record->showroom_hotel) ? ($record->showroom_hotel == "Yes" ? "checked":""):"";?>>
								Yes
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="showroom_hotel" value="No" <?php echo isset($record->showroom_hotel) && !empty($record->showroom_hotel) ? ($record->showroom_hotel == "No" ? "checked":""):"checked";?>>
								No
							  </label>
							  <p class="help-block"><span class="text-danger">Please select.</span></p>
						  </div>
					  </div>
					  
					  
					  <div class="form-group">
						<ul class="list-inline">
						  <li>
							<a class="btn btn-default" id="back4">Back</a>
						  </li>
						  <li>
							<!--<button type="submit" id="submit" class="btn btn-primary" value="Submit">Submit</button>-->
							<a class="btn btn-primary" id="next5">Finish</a>
						  </li>
						  <li>
							<a class="btn btn-success" name="save_5">Save</a>
						  </li>
						</ul>
					  </div>
					  
					  <div class="form-group">
						<div class="progress">
						  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
							100%
						  </div>
						</div>
					  </div>
					
					</div>
				  </div>
				</div>
			</div>
			
			
			<div role="tabpanel" class="tab-pane" id="summary">
				<div class="reg-form">
				  <div class="row">
					<div class="col-md-8">
					<div class="form-group">
					 <p>Please review your registration summary before clicking the submit button.</p>
						</div>
						<div class="form-group">
							<h2>Personal Details</h2>
							<p class="col-md-6">First Name </p><p class="col-md-6" id="summary_first_name">&nbsp;</p>
							<p class="col-md-6">Last Name </p><p class="col-md-6" id="summary_last_name">&nbsp;</p>
							<p class="col-md-6">Gender </p><p class="col-md-6" id="summary_gender">&nbsp;</p>
							<p class="col-md-6">Date of birth </p><p class="col-md-6" id="summary_birth_date">&nbsp;</p>
							<p class="col-md-6">Nationality </p><p class="col-md-6" id="summary_nationality">&nbsp;</p>
							<p class="col-md-6">Mobile Number </p><p class="col-md-6" id="summary_mobile">&nbsp;</p>
							<p class="col-md-6">Company </p><p class="col-md-6" id="summary_company">&nbsp;</p>
							<p class="col-md-6">Title </p><p class="col-md-6" id="summary_title">&nbsp;</p>
						</div>
						<div class="form-group">
							<h2>Travel Details</h2>
							<p class="col-md-6">Passport Number </p><p class="col-md-6" id="summary_passport_number">&nbsp;</p>
							<p class="col-md-6">Passport Issue Date </p><p class="col-md-6" id="summary_issue_date">&nbsp;</p>
							<p class="col-md-6">Passport Expiry Date </p><p class="col-md-6" id="summary_expiry_date">&nbsp;</p><br/>
							<h2>In-bound Flight</h2>
							<h3>Departure (From)</h3>
							<p class="col-md-6">Airport Name </p><p class="col-md-6" id="summary_arrival_from_airport">&nbsp;</p>
							<p class="col-md-6">Date </p><p class="col-md-6" id="summary_arrival_from_date">&nbsp;</p>
							<p class="col-md-6">Time </p><p class="col-md-6" id="summary_arrival_from_time">&nbsp;</p>
							<p class="col-md-6">Flight Number </p><p class="col-md-6" id="summary_arrival_from_flight">&nbsp;</p><br/>
							<h3>Arrival (To)</h3>
							<p class="col-md-6">Airport Name </p><p class="col-md-6" id="summary_arrival_to_airport">&nbsp;</p>
							<p class="col-md-6">Date </p><p class="col-md-6" id="summary_arrival_to_date">&nbsp;</p>
							<p class="col-md-6">Time </p><p class="col-md-6" id="summary_arrival_to_time">&nbsp;</p>
							<p class="col-md-6">Flight Number </p><p class="col-md-6" id="summary_arrival_to_flight">&nbsp;</p>
							<p class="col-md-6">Airport pickup </p><p class="col-md-6" id="summary_arrival_pickup">&nbsp;</p><br/>
							<h2>Out-bound Flight</h2>
							<h3>Departure (From)</h3>
							<p class="col-md-6">Airport Name </p><p class="col-md-6" id="summary_depart_from_airport">&nbsp;</p>
							<p class="col-md-6">Date </p><p class="col-md-6" id="summary_depart_from_date">&nbsp;</p>
							<p class="col-md-6">Time </p><p class="col-md-6" id="summary_depart_from_time">&nbsp;</p>
							<p class="col-md-6">Flight Number </p><p class="col-md-6" id="summary_depart_from_flight">&nbsp;</p><br/>
							<h3>Arrival (To)</h3>
							<p class="col-md-6">Airport Name </p><p class="col-md-6" id="summary_depart_to_airport">&nbsp;</p>
							<p class="col-md-6">Date </p><p class="col-md-6" id="summary_depart_to_date">&nbsp;</p>
							<p class="col-md-6">Time </p><p class="col-md-6" id="summary_depart_to_time">&nbsp;</p>
							<p class="col-md-6">Flight Number </p><p class="col-md-6" id="summary_depart_to_flight">&nbsp;</p>
							<p class="col-md-6">Send off to airport </p><p class="col-md-6" id="summary_airport_drop">&nbsp;</p>
						</div>
						<div class="form-group">
							<h2>Hotel Details</h2>
							<p class="col-md-6">Date of check in </p><p class="col-md-6" id="summary_check_in_date">&nbsp;</p>
							<p class="col-md-6">Date of check out </p><p class="col-md-6" id="summary_check_out_date">&nbsp;</p>
							<p class="col-md-6">Early check in </p><p class="col-md-3" id="summary_early_check_in">&nbsp;</p><p class="col-md-3" id="summary_check_in_time">&nbsp;</p>
							<p class="col-md-6">Late check out </p><p class="col-md-3" id="summary_late_check_out">&nbsp;</p><p class="col-md-3" id="summary_check_out_time">&nbsp;</p>
							<h2>Additional Information</h2>
							<p class="col-md-6">Room Preference </p><p class="col-md-6" id="summary_room_prefer">&nbsp;</p>
							<p class="col-md-6">Allergy </p><p class="col-md-6" id="summary_allergy">&nbsp;</p>
							<p class="col-md-6">Dietary Preference </p><p class="col-md-3" id="summary_diet">&nbsp;</p>&nbsp;<p class="col-md-3" id="summary_diet_extra"></p>
							<p class="col-md-6">Main Course Preference </p><p class="col-md-6" id="summary_main_course">&nbsp;</p>
						</div>
						<div class="form-group">
							<h2>Participation Details</h2>
							<p class="col-md-6">Lexus AP Forum </p><p class="col-md-6" id="summary_ap_time">&nbsp;</p>
							<p class="col-md-6">Cocktail + Official Dinner </p><p class="col-md-6" id="summary_dinner_time">&nbsp;</p>
							<p class="col-md-6">Hotel to Showroom transport </p><p class="col-md-6" id="summary_showroom_transport">&nbsp;</p>
							<p class="col-md-6">Lexus Showroom Observation </p><p class="col-md-6" id="summary_observation_time">&nbsp;</p>
							<p class="col-md-6">Showroom to Airport transport </p><p class="col-md-6" id="summary_airport_transport">&nbsp;</p>
							<p class="col-md-6">Showroom to Hotel transport </p><p class="col-md-6" id="summary_showroom_hotel">&nbsp;</p>
						</div>


					  <div class="form-group">
						<ul class="list-inline">
						  <li>
							<a class="btn btn-default" id="back5">Back</a>
						  </li>
						  <li>
							<input type="hidden" name="login" value="<?php echo site_url();?>">
							<input type="hidden" name="check_session" value="<?php echo site_url('attendee/is_session_valid');?>">
							<input type="hidden" name="completed" value=1>
							<button type="submit" id="submit_form" class="btn btn-primary" value="Submit">Submit</button>
						  </li>
						</ul>
					  </div>
					  
					</div>
				  </div>
				</div>
			</div>
		  </div>
		</form> <!-- end of form-->
        </div>
      </div>
    </div>
    <!-- / .main-content -->
    <script>
		$(window).bind('beforeunload', function(){
			if (window.isFormUpdated && !window.isFormSubmitting) {
				return "Your data will be lost when page is refreshed!!";
			}
		});
	</script>
<?php   
// Load footer   
 $this->load->view('footer');