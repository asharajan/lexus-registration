<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php 

// Load header
$this->load->view('header');?>
    
    <div class="container">

      <form class="form-signin" action="attendee/login" method="post" id="sign-in">
        <h2 class="form-signin-heading">Please sign in</h2>
        <?php echo validation_errors();?>
		<!-- Show success or error messages here -->
		<?php $flash_message = $this->session->flashdata('flash_message');
		if (isset($flash_message) AND !empty($flash_message)) : ?>
		<div class="alert <?php echo isset($flash_message['alert_class']) ? "alert-{$flash_message['alert_class']}":'' ?> text-center">
			<p style="text-align:center"><?php echo isset($flash_message['message']) ? $flash_message['message']:'Something went wrong. Please refresh the page.'; ?></p>
		</div>
		<?php endif; ?>
		<!-- END Show success or error messages here -->
		<label class="sr-only">Email address</label>
        <input type="email" class="form-control" placeholder="Email address" name="username" required autofocus>
        <label class="sr-only">Password</label>
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <!--
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        -->
        <button class="btn btn-lg btn-primary btn-block">Sign in</button>
      </form>

    </div> <!-- /container -->

    <script src="<?php echo base_url();?>assets/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/script.js"></script>
  </body>
</html>