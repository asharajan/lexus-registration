<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<p>Dear <?php echo $name;?>, </p>

<p>Thank you for registering for the upcoming regional forum. Updates to your information can be made via the following link: <a href="<?php echo site_url("/attendee/edit/{$guid}");?>" target="_blank"><?php echo site_url("/attendee/edit/{$guid}");?></a></p>

<p> As a reminder, your user name is <?php echo $this->session->userdata('username');?>. Your password is <i>Lexus_register</i>.</p>
<p> We look forward to seeing you at the event.</p>
