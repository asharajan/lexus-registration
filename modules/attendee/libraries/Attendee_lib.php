<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Attendee_lib {
	
	protected $CI;
	
	public function __construct()
	{
		$this->CI =& get_instance();
		
		$this->CI->load->model('attendee/attendee_model');
	}
	
	public function get($args = "")
	{
		// get userdata
		$result = $this->CI->attendee_model->get($args);
		
		return $result->result();
	}
	
	public function getAttendees($args = "")
	{
		// get userdata who completed the registration
		$args['completed'] = 1;
		$result = $this->CI->attendee_model->getAttendees($args);
		
		return $result->result();
	}
	
	public function saveSession($args = "")
	{
		// save data to the session in database
		if (empty($args))
			return false;

		$this->CI->session->set_userdata($args);
		
		return true;
	}
	
	public function hasRegistered($args = "")
	{
		if (empty($args)) 
			return;
		$result = $this->CI->attendee_model->get($args);
		
		return $result->result();
	}
	
	private function getEvents($args)
	{
		$events = $args['ap_forum'] == 'Yes'? "Lexus AP Forum,":"";
		$events .= $args['cocktail_dinner'] == 'Yes'? " Cocktail & Official Dinner,":"";
		$events .= $args['hotel_showroom'] == 'Yes'? " Hotel to Showroom Transport,":"";
		$events .= $args['showroom_observe'] == 'Yes'? " Lexus Showroom Observation,":"";
		$events .= $args['showroom_airport'] == 'Yes'? " Showroom to Airport Transport":"";
		
		return $events;
	}
	
	private function sendEmail($data = '')
	{		
		$this->CI->load->library('email');
		$this->CI->email->from('angela.seah@lexus-asia.com');
		$this->CI->email->to($this->CI->session->userdata('username'));
		$this->CI->email->subject('Online Registration Confirmation: Lexus Asia Pacific Forum');
		
		$html = $this->CI->load->view('email-template',$data,true);
		$this->CI->email->message($html);
		
		$sent = $this->CI->email->send();
		//echo $this->CI->email->print_debugger();
        $this->CI->email->clear();

		return $sent;
	}
	
	private function getGUID()
	{
		if (function_exists('com_create_guid')){
			return com_create_guid();
		}
		else {
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = chr(123)// "{"
				.substr($charid, 0, 8).$hyphen
				.substr($charid, 8, 4).$hyphen
				.substr($charid,12, 4).$hyphen
				.substr($charid,16, 4).$hyphen
				.substr($charid,20,12)
				.chr(125);// "}"
			return $uuid;
		}
	}
	
	public function insert($user_data)
	{
		//$user_data = $this->CI->session->all_userdata();
		foreach ($user_data as $data) {
			$this->CI->security->xss_clean($data) ;
		}
		
		$data = array(
			"first_name" => $user_data['first_name'],
			"last_name" => $user_data['last_name'],
			"gender" => $user_data['gender'],
			"birth_date" => date('Y-m-d', strtotime(str_replace("/","-",$user_data['birth_date']))),
			"nationality" => $user_data['nationality'],
			"mobile" => $user_data['mobile'],
			"company" => $user_data['company'],
			"title" => $user_data['title'],
			"behalf_of" => $user_data['behalf_of'],
			"passport_number" => $user_data['passport_number'],
			"issue_date" => !empty($user_data['issue_date'])? date('Y-m-d', strtotime(str_replace("/","-",$user_data['issue_date']))):NULL,
			"expiry_date" => !empty($user_data['expiry_date'])? date('Y-m-d', strtotime(str_replace("/","-",$user_data['expiry_date']))):NULL,
			"require_visa" => isset($user_data['require_visa'])?$user_data['require_visa']:"",
			"arrival_from_airport" => $user_data['arrival_from_airport'],
			"arrival_from_date" => !empty($user_data['arrival_from_date'])?date('Y-m-d', strtotime(str_replace("/","-",$user_data['arrival_from_date']))):NULL,
			"arrival_from_flight" => $user_data['arrival_from_flight'],
			//"arrival_from_time" => $user_data['arrival_from_hour'].":".$user_data['arrival_from_min']. "" . $user_data['arrival_from_time'],
			"arrival_from_time" => !empty($user_data['arrival_from_date'])?$user_data['arrival_from_time']:NULL,
			"arrival_to_airport" => $user_data['arrival_to_airport'],
			"arrival_to_date" => !empty($user_data['arrival_to_date'])?date('Y-m-d', strtotime(str_replace("/","-",$user_data['arrival_to_date']))):NULL,
			"arrival_to_flight" => $user_data['arrival_to_flight'],
			//"arrival_to_time" => $user_data['arrival_to_hour'].":".$user_data['arrival_to_min']. "" . $user_data['arrival_to_time'],
			"arrival_to_time" => !empty($user_data['arrival_to_date'])?$user_data['arrival_to_time']:NULL,
			"depart_from_airport" => $user_data['depart_from_airport'],
			"depart_from_date" => !empty($user_data['depart_from_date'])?date('Y-m-d', strtotime(str_replace("/","-",$user_data['depart_from_date']))):NULL,
			"depart_from_flight" => $user_data['depart_from_flight'],
			//"depart_from_time" => $user_data['depart_from_hour'].":".$user_data['depart_from_min']. "" . $user_data['depart_from_time'],
			"depart_from_time" =>  !empty($user_data['depart_from_date'])?$user_data['depart_from_time']:NULL,
			"depart_to_airport" => $user_data['depart_to_airport'],
			"depart_to_date" => !empty($user_data['depart_to_date'])?date('Y-m-d', strtotime(str_replace("/","-",$user_data['depart_to_date']))):NULL,
			"depart_to_flight" => $user_data['depart_to_flight'],
			//"depart_to_time" => $user_data['depart_to_hour'].":".$user_data['depart_to_min']. "" . $user_data['depart_to_time'],
			"depart_to_time" => !empty($user_data['depart_to_date'])?$user_data['depart_to_time']:NULL,
			"arrival_pickup" => isset($user_data['arrival_pickup'])?$user_data['arrival_pickup']:"",
			"airport_drop" => isset($user_data['airport_drop'])?$user_data['airport_drop']:"",
			"early_check_in" => isset($user_data['early_check_in'])?$user_data['early_check_in']:"",
			//"check_in_time" => $user_data['check_in_hour'].":".$user_data['check_in_min']. "" . $user_data['check_in_time'],
			"check_in_time" => !empty($user_data['early_check_in']) && ($user_data['early_check_in'] == "Yes")?$user_data['check_in_time']:NULL,
			"check_in_date" => !empty($user_data['check_in_date'])?date('Y-m-d', strtotime(str_replace("/","-",$user_data['check_in_date']))):NULL,
			"check_out_date" => !empty($user_data['check_out_date'])?date('Y-m-d', strtotime(str_replace("/","-",$user_data['check_out_date']))):NULL,
			"late_check_out" => isset($user_data['late_check_out'])?$user_data['late_check_out']:"",
			//"check_out_time" => $user_data['check_out_hour'].":".$user_data['check_out_min']. "" . $user_data['check_out_time'],
			"check_out_time" => !empty($user_data['late_check_out']) && ($user_data['late_check_out'] == "Yes")?$user_data['check_out_time']:NULL,
			"room" => isset($user_data['room_prefer'])?$user_data['room_prefer']:"",
			"diet" => $user_data['diet'],
			"diet_extra" => $user_data['diet_extra'],
			"allergy" => $user_data['allergy'],
			"main_course" => $user_data['main_course'],
			"main_course_extra" => $user_data['main_course_extra'],
			"ap_forum" => $user_data['ap_time'],
			"cocktail_dinner" => $user_data['dinner_time'],
			"hotel_showroom" => $user_data['showroom_transport'],
			"showroom_observe" => $user_data['observation_time'],
			"showroom_airport" => $user_data['airport_transport'],
			"showroom_hotel" => $user_data['showroom_hotel'],
			"completed" => $user_data['completed'],
			"registration_date" => date('Y-m-d H:i:s'),
		);
		// run update query if user has already registered
		$user_id = $this->CI->session->userdata('user_id');
		$args['user_id'] = $user_id;
		if (($user = $this->hasRegistered($args)) != null) {
			// if already registered, dont change the value of completed field
			$data['completed'] = $user[0]->completed;
			$result = $this->CI->attendee_model->update($args,$data);
			// if user clicked on save button
			if ($user_data['completed'] == 0) {
				// handle if user saved the form
				$this->CI->session->set_flashdata('flash_message', array(
					'alert_class'	=> 'success',
					'message'	=> 'Your details have been saved successfully.',
				));
			}
		} else {
			//$guid = com_create_guid();
			$guid = $this->getGUID();
			$data['user_id'] = $user_id;
			$data['guid'] = $guid;
			$result = $this->CI->attendee_model->insert($data);
			// send mail only if form completed
			if ($user_data['completed'] == 1) {
				if ($result) {
					//send email
					$mail_data['events'] = $this->getEvents($data);
					$mail_data['name'] = $data['first_name'] . " " . $data['last_name'];
					$mail_data['guid'] = $data['guid'];
					$sent = $this->sendEmail($mail_data);
				} else {
					//print_r($result);
				}
			} else {
				// handle if user saved the form
				$this->CI->session->set_flashdata('flash_message', array(
					'alert_class'	=> 'success',
					'message'	=> 'Your details have been saved successfully.',
				));
			}
		}
		return $result;
	}
}