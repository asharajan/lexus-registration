<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->register();
	}
	
	public function register() {
		// check whether user is validated or not
		$validated = $this->session->userdata("admin_validated");
		
		// if validated, then load the registration
		if ($validated) 
		{
			$this->load->view("admin/dashboard");
		} 
		else 
		{
			$this->load->view("admin/login");
		}
		
	}
	
	public function login()
	{
		
		$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert" style="margin-left: 30%;margin-right: 30%; text-align: center">', '</div>');		
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		
		if (TRUE == $this->form_validation->run()) {
			$password = $this->input->post('password',TRUE);
			$username = $this->input->post('username',TRUE);
			if ("Lexus_register" == $password && "Lexus" == $username) {
				$this->session->set_userdata("admin_validated",true);
				$this->session->set_userdata("user_id","-1");
				$this->session->set_userdata("username","Admin");
				redirect('admin/dashboard');
			} else {
				// set flashdata
				$this->session->set_flashdata('flash_message', array(
							'alert_class'	=> 'danger',
							'message'	=> 'Login failed!',
						));
				redirect('admin');
			}
		} else {
			$this->load->view('admin/login');
		}
	}
	
	public function dashboard() 
	{
		$data = array();
		$this->load->library('attendee/attendee_lib');
		
		$result = $this->attendee_lib->getAttendees();
		$data['result'] = $result;
		$this->load->view('admin/dashboard',$data);
	}
	
	
	public function export_data()
	{
		$this->load->library('excel');
		$this->load->helper('file');
		$this->load->helper('download');
		
		$name = "Attendee_List_".date('d-M-y');
		$filename = "{$name}.xls";
		
		$query = "SELECT attendee.*, users.username from attendee,users WHERE users.user_id= attendee.user_id AND completed=1 ORDER BY registration_date";
		
		$result = $this->db->query($query);
		$data['result'] = $result;
		$data['filename'] = $filename;
		$excel = $this->load->view('excel-attendee',$data,TRUE);
		
		force_download($filename, $excel);
		
	}
	
}
