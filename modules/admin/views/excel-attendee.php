<?php
		
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);

		//name the worksheet
		$this->excel->getActiveSheet()->setTitle($filename);
		$this->excel->getActiveSheet()->getStyle("A1:AT1")->getFont()->setBold(true);

		// set header
		$this->excel->getActiveSheet()->setCellValue('A1', 'No:');
		$this->excel->getActiveSheet()->setCellValue('B1', 'First Name');
		$this->excel->getActiveSheet()->setCellValue('C1', 'Last Name');
		$this->excel->getActiveSheet()->setCellValue('D1', 'Gender');
		$this->excel->getActiveSheet()->setCellValue('E1', 'Date of birth');
		$this->excel->getActiveSheet()->setCellValue('F1', 'Nationality');
		$this->excel->getActiveSheet()->setCellValue('G1', 'Mobile');
		$this->excel->getActiveSheet()->setCellValue('H1', 'Email Address');
		$this->excel->getActiveSheet()->setCellValue('I1', 'Company');
		$this->excel->getActiveSheet()->setCellValue('J1', 'Title');
		$this->excel->getActiveSheet()->setCellValue('K1', 'Passport Number');
		$this->excel->getActiveSheet()->setCellValue('L1', 'Issue Date');
		$this->excel->getActiveSheet()->setCellValue('M1', 'Expiry Date');
		$this->excel->getActiveSheet()->setCellValue('N1', 'Require Visa');
		$this->excel->getActiveSheet()->setCellValue('O1', 'Arrival from airport');
		$this->excel->getActiveSheet()->setCellValue('P1', 'Arrival from date');
		$this->excel->getActiveSheet()->setCellValue('Q1', 'Arrival from flight number');
		$this->excel->getActiveSheet()->setCellValue('R1', 'Arrival from time');
		$this->excel->getActiveSheet()->setCellValue('S1', 'Arrival to airport');
		$this->excel->getActiveSheet()->setCellValue('T1', 'Arrival to date');
		$this->excel->getActiveSheet()->setCellValue('U1', 'Arrival to flight number');
		$this->excel->getActiveSheet()->setCellValue('V1', 'Arrival to time');
		$this->excel->getActiveSheet()->setCellValue('W1', 'Arrival pickup');
		$this->excel->getActiveSheet()->setCellValue('X1', 'Depart from airport');
		$this->excel->getActiveSheet()->setCellValue('Y1', 'Depart from date');
		$this->excel->getActiveSheet()->setCellValue('Z1', 'Depart from flight');
		$this->excel->getActiveSheet()->setCellValue('AA1', 'Depart from time');
		$this->excel->getActiveSheet()->setCellValue('AB1', 'Depart to airport');
		$this->excel->getActiveSheet()->setCellValue('AC1', 'Depart to date');
		$this->excel->getActiveSheet()->setCellValue('AD1', 'Depart to flight');
		$this->excel->getActiveSheet()->setCellValue('AE1', 'Depart to time');
		$this->excel->getActiveSheet()->setCellValue('AF1', 'Airport drop');
		$this->excel->getActiveSheet()->setCellValue('AG1', 'Early check in');
		$this->excel->getActiveSheet()->setCellValue('AH1', 'Check in time');
		$this->excel->getActiveSheet()->setCellValue('AI1', 'Check in date');
		$this->excel->getActiveSheet()->setCellValue('AJ1', 'Check out date');
		$this->excel->getActiveSheet()->setCellValue('AK1', 'Late check out');
		$this->excel->getActiveSheet()->setCellValue('AL1', 'Check out time');
		$this->excel->getActiveSheet()->setCellValue('AM1', 'Room preference');
		$this->excel->getActiveSheet()->setCellValue('AN1', 'Allergy');
		$this->excel->getActiveSheet()->setCellValue('AO1', 'Diet');
		$this->excel->getActiveSheet()->setCellValue('AP1', 'Diet - Other Preference');
		$this->excel->getActiveSheet()->setCellValue('AQ1', 'Main Course Preference');
		$this->excel->getActiveSheet()->setCellValue('AR1', 'Main Course Other Preference');
		$this->excel->getActiveSheet()->setCellValue('AS1', 'Lexus AP Forum');
		$this->excel->getActiveSheet()->setCellValue('AT1', 'Cocktail + Official Dinner');
		$this->excel->getActiveSheet()->setCellValue('AU1', 'Hotel to Showroom transport');
		$this->excel->getActiveSheet()->setCellValue('AV1', 'Lexus Showroom Observation');
		$this->excel->getActiveSheet()->setCellValue('AW1', 'Showroom to Airport transport');
		$this->excel->getActiveSheet()->setCellValue('AX1', 'Showroom to Hotel transport');
		
		// adding data to row
		$rowctr = 2;

		foreach($result->result() as $row)
		{
			$this->excel->getActiveSheet()->setCellValue('A' . $rowctr, $rowctr-1);
			$this->excel->getActiveSheet()->setCellValue('B' . $rowctr, $row->first_name);
			$this->excel->getActiveSheet()->setCellValue('C' . $rowctr, $row->last_name);
			$this->excel->getActiveSheet()->setCellValue('D' . $rowctr, $row->gender);
			$this->excel->getActiveSheet()->setCellValue('E' . $rowctr, date('d/m/Y', strtotime($row->birth_date)));
			$this->excel->getActiveSheet()->setCellValue('F' . $rowctr, $row->nationality);	
			$this->excel->getActiveSheet()->setCellValue('G' . $rowctr, $row->mobile);	
			$this->excel->getActiveSheet()->setCellValue('H' . $rowctr, $row->username);	
			$this->excel->getActiveSheet()->setCellValue('I' . $rowctr, $row->company);
			$this->excel->getActiveSheet()->setCellValue('J' . $rowctr, $row->title);
			$this->excel->getActiveSheet()->setCellValue('K' . $rowctr, $row->passport_number);
			$this->excel->getActiveSheet()->setCellValue('L' . $rowctr, !empty($row->issue_date)?date('d/m/Y', strtotime($row->issue_date)):"");
			$this->excel->getActiveSheet()->setCellValue('M' . $rowctr, !empty($row->expiry_date)?date('d/m/Y', strtotime($row->expiry_date)):"");
			$this->excel->getActiveSheet()->setCellValue('N' . $rowctr, $row->require_visa);
			$this->excel->getActiveSheet()->setCellValue('O' . $rowctr, $row->arrival_from_airport);
			$this->excel->getActiveSheet()->setCellValue('P' . $rowctr, !empty($row->arrival_from_date)?date('d/m/Y', strtotime($row->arrival_from_date)):"");
			$this->excel->getActiveSheet()->setCellValue('Q' . $rowctr, $row->arrival_from_flight);
			$this->excel->getActiveSheet()->setCellValue('R' . $rowctr, !empty($row->arrival_from_time)?$row->arrival_from_time:"");
			$this->excel->getActiveSheet()->setCellValue('S' . $rowctr, $row->arrival_to_airport);
			$this->excel->getActiveSheet()->setCellValue('T' . $rowctr, !empty($row->arrival_to_date)?date('d/m/Y', strtotime($row->arrival_to_date)):"");
			$this->excel->getActiveSheet()->setCellValue('U' . $rowctr, $row->arrival_to_flight);
			$this->excel->getActiveSheet()->setCellValue('V' . $rowctr, !empty($row->arrival_to_time)?$row->arrival_to_time:"");
			$this->excel->getActiveSheet()->setCellValue('W' . $rowctr, $row->arrival_pickup);
			$this->excel->getActiveSheet()->setCellValue('X' . $rowctr, $row->depart_from_airport);
			$this->excel->getActiveSheet()->setCellValue('Y' . $rowctr, !empty($row->depart_from_date)?date('d/m/Y', strtotime($row->depart_from_date)):"");
			$this->excel->getActiveSheet()->setCellValue('Z' . $rowctr, $row->depart_from_flight);
			$this->excel->getActiveSheet()->setCellValue('AA' . $rowctr, !empty($row->depart_from_time)?$row->depart_from_time:"");
			$this->excel->getActiveSheet()->setCellValue('AB' . $rowctr, $row->depart_to_airport);
			$this->excel->getActiveSheet()->setCellValue('AC' . $rowctr, !empty($row->depart_to_date)?date('d/m/Y', strtotime($row->depart_to_date)):"");
			$this->excel->getActiveSheet()->setCellValue('AD' . $rowctr, $row->depart_to_flight);
			$this->excel->getActiveSheet()->setCellValue('AE' . $rowctr, !empty($row->depart_to_time)?$row->depart_to_time:"");
			$this->excel->getActiveSheet()->setCellValue('AF' . $rowctr, $row->airport_drop);
			$this->excel->getActiveSheet()->setCellValue('AG' . $rowctr, $row->early_check_in);
			$this->excel->getActiveSheet()->setCellValue('AH' . $rowctr, !empty($row->check_in_time)?$row->check_in_time:"");
			$this->excel->getActiveSheet()->setCellValue('AI' . $rowctr, !empty($row->check_in_date)?date('d/m/Y', strtotime($row->check_in_date)):"");
			$this->excel->getActiveSheet()->setCellValue('AJ' . $rowctr, !empty($row->check_out_date)?date('d/m/Y', strtotime($row->check_out_date)):"");
			$this->excel->getActiveSheet()->setCellValue('AK' . $rowctr, $row->late_check_out);
			$this->excel->getActiveSheet()->setCellValue('AL' . $rowctr, !empty($row->check_out_time)?$row->check_out_time:"");
			$this->excel->getActiveSheet()->setCellValue('AM' . $rowctr, $row->room);
			$this->excel->getActiveSheet()->setCellValue('AN' . $rowctr, $row->allergy);
			$this->excel->getActiveSheet()->setCellValue('AO' . $rowctr, $row->diet);
			$this->excel->getActiveSheet()->setCellValue('AP' . $rowctr, $row->diet_extra);
			$this->excel->getActiveSheet()->setCellValue('AQ' . $rowctr, $row->main_course);
			$this->excel->getActiveSheet()->setCellValue('AR' . $rowctr, $row->main_course_extra);
			$this->excel->getActiveSheet()->setCellValue('AS' . $rowctr, $row->ap_forum);
			$this->excel->getActiveSheet()->setCellValue('AT' . $rowctr, $row->cocktail_dinner);
			$this->excel->getActiveSheet()->setCellValue('AU' . $rowctr, $row->hotel_showroom);
			$this->excel->getActiveSheet()->setCellValue('AV' . $rowctr, $row->showroom_observe);
			$this->excel->getActiveSheet()->setCellValue('AW' . $rowctr, $row->showroom_airport);
			$this->excel->getActiveSheet()->setCellValue('AX' . $rowctr, $row->showroom_hotel);
			$rowctr++;
		}
		
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');