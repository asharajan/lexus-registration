<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php 

// Load header
$this->load->view('header');?>
	<div class="main-container container">
		<h1>Lexus Asia Pacific Online Registration</h1>
		<form action="admin/export_data" method="post">
			<div class="form-group">
				<p>Click to download registration details: &nbsp;<button name="import" class="btn btn-primary pull-right">Export Details</button></p>
			</div>
		</form>
		
		<div class="row">
			<div class="col-md-12">
				<div class="table-responsive">
					<table class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Name</th>
								<th>Gender</th>
								<th>Mobile</th>
								<th>Date of birth</th>
								<th>Nationality</th>
								<th>Company</th>
								<th>Title</th>
							</tr>
						</thead>
						<tbody>
							<?php if (isset($result) AND ! empty($result)) : ?>
								<?php foreach ($result as $row) : ?>
								<tr>
									<td><?php echo "{$row->first_name} {$row->last_name}"; ?></td>
									<td><?php echo $row->gender; ?></td>
									<td><?php echo $row->mobile; ?></td>
									<td><?php echo date('d/m/Y', strtotime($row->birth_date)); ?></td>
									<td><?php echo $row->nationality; ?></td>
									<td><?php echo $row->company; ?></td>
									<td><?php echo $row->title; ?></td>
								</tr>
								<?php endforeach; ?>
							<?php else: ?>
							<tr>
								<td colspan="7">- no data -</td>
							</tr>
							<?php endif; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php   
// Load footer   
 $this->load->view('footer');