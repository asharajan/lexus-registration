/* Lexus Registration */

$(function() {
	
	function check_session() {
		$.ajax({
				type: 'POST',
				url: $('input[name=check_session]').val(),
				dataType: 'json',
				success: function(res) {
					if (!res.response)
						window.location.href=$('input[name=login]').val();
				}
			});
		return true;
	}
	
	function isValidDate(dateString)
	{
		// First check for the pattern
		if(!/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(dateString))
			return false;

		var parts = dateString.split("/");
		var day = parseInt(parts[0], 10);
		var month = parseInt(parts[1], 10);
		var year = parseInt(parts[2], 10);

		// Check the ranges of month and year
		if(year < 1000 || year > 3000 || month == 0 || month > 12)
			return false;

		var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

		// Adjust for leap years
		if(year % 400 == 0 || (year % 100 != 0 && year % 4 == 0))
			monthLength[1] = 29;

		// Check the range of the day
		return day > 0 && day <= monthLength[month - 1];
	}

	function check_required(required){
		// check if session is valid 
		if (check_session()) { 
			$('.'+ required).each(function(){
				if ($(this).val() == '') {
					$(this).siblings('.help-block').show();
					//alert($(this).attr('name'));
					$(this).addClass('invalid');
				} else {
					$(this).siblings('.help-block').hide();
					$(this).removeClass('invalid');
				}
			});
			
			if (required == 'required_1') {
				format_check('birth_date');
			}
			
			if (required == 'required_2') {
				format_check('issue_date');
				format_check('expiry_date');
			}

			if ($('.invalid').size() == 0) {
				return true;
			} else {
				$('html, body').animate({
					scrollTop: $(".invalid:first").offset().top
				}, 200);
				return false;
			}
		}
	}
	
	function format_check(date_field)
	{
		var date = $('input[name='+date_field+']').val();
		if (date != '') {
			if(date.indexOf('-') > 0) {
				 $('input[name='+date_field+']').siblings('.help-block.incorrect').show();
				 $('input[name='+date_field+']').addClass('invalid');
			} else {
				 if (isValidDate(date)) {
					 $('input[name='+date_field+']').siblings('.help-block.incorrect').hide();
					 $('input[name='+date_field+']').removeClass('invalid');
				 } else {
					 $('input[name='+date_field+']').siblings('.help-block.incorrect').show();
					 $('input[name='+date_field+']').addClass('invalid');
				 }
			}
		}
	}
	
	function check_date(departure,arrival)
	{
		var parts = departure.split("/");
		var depart = new Date(parts[2], parts[1] - 1, parts[0]);
		var parts = arrival.split("/");
		var arrive = new Date(parts[2], parts[1] - 1, parts[0]);
		
		if (depart <= arrive) 
			return true;
		else
			return false;
	}
		
  $(document).ready(function(){
		
		// datepicker
		$('.datepicker').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true
		});	 

		$('.timepicker').timepicker({
			maxHours: 24,
			minuteStep: 1,
			defaultTime: '12:00 AM'
		});	 
		
		// trigger 'Please specify'
		$('select[name=main_course]').change(function(){
			if ($(this).val() == "Others") {
				$('input[name=main_course_extra]').removeClass('hidden');
			} else {
				$('input[name=main_course_extra]').addClass('hidden');
				$('input[name=main_course_extra]').val('');
			}
		});
		
		$('input[name=early_check_in],input[name=late_check_out]').change(function(e) {
			var dataShowField = $(this).data('show-field');
			var dataHideField = $(this).data('hide-field');
						
			if (dataShowField) {
				if ($(this).prop('checked') == true) {
					$('[name=' + dataShowField + ']').closest('.form-group').removeClass('hidden');
				} else {
					$('[name=' + dataShowField + ']').closest('.form-group').addClass('hidden');
					$('[name=' + dataShowField + ']').timepicker('setTime', '12:00 AM');
				}
			} 
			if (dataHideField) {
				if ($(this).prop('checked') == true) {
					$('[name=' + dataHideField + ']').closest('.form-group').addClass('hidden');
					$('[name=' + dataHideField + ']').timepicker('setTime', '12:00 AM');
				} else {
					$('[name=' + dataShowField + ']').closest('.form-group').removeClass('hidden');
				}
			}
		});

		// restrict the to-from date 
		$('input[name=arrival_from_date],input[name=check_in_date]').datepicker().on('changeDate',function(e){
			if ($(this).attr('name') == "arrival_from_date") {
				var minDate = new Date(e.date.valueOf());
				$('input[name=arrival_to_date]').datepicker('setStartDate', minDate);
			}
			else if ($(this).attr('name') == "arrival_to_date") {
				var minDate = new Date(e.date.valueOf());
				$('input[name=depart_from_date]').datepicker('setStartDate', minDate);
			}
			/* else if ($(this).attr('name') == "depart_from_date") {
				var minDate = new Date(e.date.valueOf());
				$('input[name=depart_to_date]').datepicker('setStartDate', minDate);
			} */
			else if ($(this).attr('name') == "check_in_date") {
				var minDate = new Date(e.date.valueOf());
				$('input[name=check_out_date]').val('');
				$('input[name=check_out_date]').datepicker('setStartDate', minDate);
			}
			else if ($(this).attr('name') == "issue_date") {
				var minDate = new Date(e.date.valueOf());
				$('input[name=expiry_date]').val('');
				$('input[name=expiry_date]').datepicker('setStartDate', minDate);
			}
			
		});
		
		$('input[name=airport_transport],input[name=showroom_hotel]').on('change',function(e){
			if ($(this).attr('name') == "airport_transport") {
				if ($(this).val() == "Yes") {
					$('input[name=showroom_hotel][value=No]').prop('checked',true);
				} else {
					$('input[name=showroom_hotel][value=Yes]').prop('checked',true);
				}
			}
			else if ($(this).attr('name') == "showroom_hotel") {
				if ($(this).val() == "Yes") {
					$('input[name=airport_transport][value=No]').prop('checked',true);
				} else {
					$('input[name=airport_transport][value=Yes]').prop('checked',true);
				}
			}
		});
	  
		$('form input, form select, form textarea').change(function() {
			window.isFormUpdated = true;
		});
		$('p.help-block').hide();
		
		// move between forms
		$('#next1').click(function(){
			//alert(check_required('required_1'));
			if (check_required('required_1')) {
				$('#steps a[href="#form_2"]').tab('show');
				$('html, body').animate({
					scrollTop: $("#form_2").offset().top
				}, 200);
			}
		});
		$('#next2').click(function(){
			var arrival_from_date = $('input[name=arrival_from_date]').val();
			var arrival_to_date = $('input[name=arrival_to_date]').val();
			var issue_date = $('input[name=issue_date]').val();
			var expiry_date = $('input[name=expiry_date]').val();
			
			if (check_required('required_2')) {
				// passport expiry
				if (issue_date != '' || expiry_date != '') {
					if (!check_date(issue_date,expiry_date)) {
						$('input[name=issue_date]').siblings('.incorrect_date').show();
						$('html, body').animate({
							scrollTop: $('input[name=issue_date]').offset().top
						}, 200);
						return;
					} else {
						$('input[name=issue_date]').siblings('.help-block').hide();
					}
				}
				$('#steps a[href="#form_3"]').tab('show');
				$('html, body').animate({
					scrollTop: $("#form_3").offset().top
				}, 200);
			}
		});
		
		$('#next3').click(function(){
			var depart_from_date = $('input[name=depart_from_date]').val();
			var depart_to_date = $('input[name=depart_to_date]').val();
			if (check_required('required_3')) {
				$('#steps a[href="#form_4"]').tab('show');
				$('html, body').animate({
					scrollTop: $("#form_4").offset().top
				}, 200);
			}
		});
		$('#next4').click(function(){
			var check_in_date = $('input[name=check_in_date]').val();
			var check_out_date = $('input[name=check_out_date]').val();
			if (check_required('required_4')) {	
				var diet = "";
				$('input[name="diet[]"]:checked').each(function(){
					diet += $(this).val() + ",";
				});
				$('input[name=diet]').val(diet);
				// check-in and check-out
				if (check_in_date != '' || check_out_date != '') {
					if (!check_date(check_in_date,check_out_date)) {
						$('input[name=check_in_date]').siblings('.incorrect_date').show();
						$('html, body').animate({
							scrollTop: $('input[name=check_in_date]').offset().top
						}, 200);
						return;
					} else {
						$('input[name=check_in_date]').siblings('.help-block').hide();
					}
				}
				$('#steps a[href="#form_5"]').tab('show');
				$('html, body').animate({
					scrollTop: $("#form_5").offset().top
				}, 200);
			}
		});
		
		$('#next5').click(function(){
			var formArray = $("#registration_form").serializeArray();
			//$('#form-content').text(formArray);
			
			if (check_required('required_5')) {	
				// fill the summary tab
				for(i=0;i<formArray.length;i++) {
					if (formArray[i].name == "diet[]") {
						continue;
					}else if (formArray[i].name == "check_in_time") { // show only if early check in selected
						if ($('[name="early_check_in"]:checked').val() == "Yes")
							$('#summary_'+formArray[i].name).html( formArray[i].name.replace(/_/g,' ') + ": "+formArray[i].value);
						
					}else if (formArray[i].name == "check_out_time") { // show only if late check out selected
						if ($('[name="late_check_out"]:checked').val() == "Yes")
							$('#summary_'+formArray[i].name).html( formArray[i].name.replace(/_/g,' ') + ": "+formArray[i].value);
						
					} else if ((formArray[i].name).indexOf("_time") > 0) { // show only if date fields have value
						var parts = (formArray[i].name).split("_");
						parts[2] = "date";
						var field = parts.join("_");
						if ($('[name='+field+']').val() !== '') {
							$('#summary_'+formArray[i].name).html(": "+formArray[i].value);
						}						
					}else 						
						$('#summary_'+formArray[i].name).html(": "+formArray[i].value);
				}
				$('#steps a[href="#summary"]').tab('show');
				$('html, body').animate({
					scrollTop: $("#summary").offset().top
				}, 200);
			} 
		});
		
		$('#submit_form').click(function(){
			window.isFormSubmitting = true;
			form.submit();
		});
		
		$('#back1').click(function(){
			$('#steps a[href="#form_1"]').tab('show');
			$('html, body').animate({
				scrollTop: $("#form_1").offset().top
			}, 200);
		});
		$('#back2').click(function(){
			$('#steps a[href="#form_2"]').tab('show');
			$('html, body').animate({
				scrollTop: $("#form_2").offset().top
			}, 200);
		});
		$('#back3').click(function(){
			$('#steps a[href="#form_3"]').tab('show');
			$('html, body').animate({
				scrollTop: $("#form_3").offset().top
			}, 200);
		});
		$('#back4').click(function(){
			$('#steps a[href="#form_4"]').tab('show');
			$('html, body').animate({
				scrollTop: $("#form_4").offset().top
			}, 200);
		});
		$('#back5').click(function(){
			$('#steps a[href="#form_5"]').tab('show');
			$('html, body').animate({
				scrollTop: $("#form_5").offset().top
			}, 200);
		});
		
		
		$('[name^=save]').click(function(){
			// get current section and validate
			var validate_page = $(this).attr('name');
			validate_page = validate_page.substr(validate_page.length - 1);
			if (check_required('required_'+validate_page)) {
				$('input[name=completed]').val(0);
				console.log('submitting');
				window.isFormSubmitting = true;
				$('#registration_form').submit();
			}
		});
		
		var navHeight = $('.navbar').outerHeight();
		var footerHeight = $('.footer').outerHeight();
		var containerHeight = $('.main-container').outerHeight();
		var viewportHeight = $(window).height();
		
		if (containerHeight < viewportHeight-(navHeight+footerHeight)) {
			$('.main-container').height(viewportHeight-(navHeight+footerHeight)-20);
		}
	});
});